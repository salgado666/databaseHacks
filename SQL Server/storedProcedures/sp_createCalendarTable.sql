CREATE OR ALTER PROCEDURE dbo.sp_createCalendarTable
    @schemaName        NVARCHAR(128),
    @tableName         NVARCHAR(128),
    @startDateTime     DATETIME,
    @endDateTime       DATETIME,
    @intervalInMinutes INT = 15
AS
BEGIN

    SET NOCOUNT ON

    /*************************************************************************************
    * DATABASE   : sqldb-australiaeast-rf-smartwater-qa01
    * OBJECT NAME: [dbo].[sp_createCalendarTable]
    * WorkOrder  : 
    * DATE       : 2023-06-22
    * CREATED BY : Daniel Salgado
    * DESCRIPTION: Creates a calendar table and puplulates it with dates and times
                   EXEC dbo.sp_createCalendarTable @schemaName = 'np2m', @tableName = 'dqc_calendar', @startDateTime = '2023-01-01', @endDateTime = '2023-12-31', @intervalInMinutes = 15;
    **************************************************************************************/

    -- Create dynamic SQL statement
    DECLARE @sql NVARCHAR(MAX);

    SET @sql = N'
    -- Create Calendar table
    IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''' + @schemaName + ''' AND TABLE_NAME = ''' + @tableName + ''')
    BEGIN
        CREATE TABLE ' + QUOTENAME(@schemaName) + '.' + QUOTENAME(@tableName) + ' (
            [datetimeId]     BIGINT NOT NULL,
            [dateTime]       DATETIME,
            [date]           DATE,
            [year]           INT,
            [quarter]        INT,
            [month]          INT,
            [monthName]      VARCHAR(20),
            [monthShortName] VARCHAR(10),
            [monthYear]      VARCHAR(8),
            [yearMonth]      VARCHAR(7),
            [weekOfYear]     INT,
            [dayOfWeek]      INT,
            [dayOfYear]      INT,
            [dayName]        VARCHAR(20),
            [hour]           INT,
            [minute]         INT,
            CONSTRAINT PK_CALTABLE PRIMARY KEY (datetimeId)
        );
    END;

    -- Populate Calendar table with data
    WITH CalendarData AS (
        SELECT
            [datetimeId] = CAST(FORMAT([dateTime],''yyyyMMddHHmmss'') AS BIGINT),
            [dateTime],
            [date] = CAST([dateTime] AS DATE)
        FROM
            (
            SELECT TOP (DATEDIFF(MINUTE, ''' + CONVERT(NVARCHAR(MAX), @startDateTime, 121) + ''', ''' + CONVERT(NVARCHAR(MAX), @endDateTime, 121) + ''') / ' + CAST(@intervalInMinutes AS NVARCHAR(MAX)) + ' + 1)
                [dateTime] = DATEADD(MINUTE, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) * ' + CAST(@intervalInMinutes AS NVARCHAR(MAX)) + ' - ' + CAST(@intervalInMinutes AS NVARCHAR(MAX)) + ', ''' + CONVERT(NVARCHAR(MAX), @startDateTime, 121) + ''')
            FROM
                sys.columns sc1, sys.columns sc2
            ) AS sub
    )
    INSERT INTO ' + QUOTENAME(@schemaName) + '.' + QUOTENAME(@tableName) + '
    SELECT
        [datetimeId],
        [dateTime],
        [date],
        [year]           = YEAR([date]),
        [quarter]        = DATEPART(QUARTER, [date]),
        [month]          = MONTH([date]),
        [monthName]      = DATENAME(MONTH, [date]),
        [monthShortName] = LEFT(DATENAME(MONTH, [date]), 3),
        [monthYear]      = FORMAT([date], ''MMM-yyyy''),
        [yearMonth]      = FORMAT([date], ''yyyyMM''),
        [weekOfYear]     = DATEPART(WEEK, [date]),
        [dayOfWeek]      = DATEPART(WEEKDAY, [date]),
        [dayOfYear]      = DATEPART(DAYOFYEAR, [date]),
        [dayName]        = DATENAME(WEEKDAY, [date]),
        [hour]           = DATEPART(HOUR, [dateTime]),
        [minute]         = DATEPART(MINUTE, [dateTime])
    FROM
        CalendarData;
    ';

    -- Execute dynamic SQL
    EXEC sp_executesql @sql;
END;
